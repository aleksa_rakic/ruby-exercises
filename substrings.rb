def substrings(sentence, dictionary)
	sentence_words = sentence.split(/\W/)
	outputhash = {}

	dictionary.each do |dict_word|
		match = 0
		sentence_words.each do |sent_word|
			if sent_word =~ (/#{dict_word}/)
				match += 1 
			end
			if match > 0
				outputhash[dict_word] = match
			end
		end
	end
	puts outputhash
end
dictionary = ["trebati", "potpuno", "sam", "Martina", "od", "tražim", "cyberpunk", "ostavio", "ti", "da", "i", "nemam", "ponešto", "i", "čoveka", "more", "sta"]

substrings("Tražeći ono što mi treba, a što ni sam ne znam tačno šta je, išao sam od čoveka do čoveka, i video sam da svi zajedno imaju manje nego što imam ja koji ništa nemam, i da sam kod svakoga ostavio ponešto od onoga što nemam i što tražim. – Ostaćeš potpuno sam i neće ti biti potrebno da umreš!", dictionary)
